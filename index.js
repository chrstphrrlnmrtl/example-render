const express = require("express");
const mongoose  = require("mongoose");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 4000;

//connect api to db:
mongoose.connect("mongodb+srv://admin:admin@batch288mortel.qn2vr6o.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

//notifications for successful or failed mongodb connection:
let db = mongoose.connection;
db.on("error",console.error.bind(console, "Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

//cors() is used to limit access to your application. With this we can allow/disallow certain applications from accessing our app.
app.use(cors());
app.use(express.json());

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);


const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);


const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);

app.listen(port,()=>console.log(`Server running at localhost:4000`));